# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

ITEM_REGISTERED = "item-registered"     # item name as str
HOST_REGISTERED = "host-registered"     # host registered
