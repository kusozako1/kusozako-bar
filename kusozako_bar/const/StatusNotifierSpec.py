# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio

WELL_KNOWN_NAME = "org.kde.StatusNotifierWatcher"
OBJECT_PATH = "/StatusNotifierWatcher"
INTERFACE = "org.kde.StatusNotifierWatcher"
INTROSPECTION_XML = """
<!DOCTYPE node PUBLIC
"-//freedesktop//DTD D-BUS Object Introspection 1.0//EN"
"http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd">
<node>
    <interface name="org.kde.StatusNotifierWatcher">
        <method name="RegisterStatusNotifierItem">
            <arg name="service" type="s" direction="in"/>
        </method>
        <method name="RegisterStatusNotifierHost">
            <arg name="service" type="s" direction="in"/>
        </method>
        <property
            name="RegisteredStatusNotifierItems"
            type="as"
            access="read"
        />
        <property
            name="IsStatusNotifierHostRegistered"
            type="b"
            access="read"
        />
        <property
            name="ProtocolVersion"
            type="i"
            access="read"
        />
        <signal name="StatusNotifierItemRegistered">
            <arg type="s" direction="out" name="service"/>
        </signal>
        <signal name="StatusNotifierItemUnregistered">
            <arg type="s" direction="out" name="service"/>
        </signal>
        <signal name="StatusNotifierHostRegistered"/>
        <signal name="StatusNotifierHostUnregistered"/>
    </interface>
</node>
"""
NODE_INFO = Gio.DBusNodeInfo.new_for_xml(INTROSPECTION_XML)
INTERFACE_INFO = NODE_INFO.lookup_interface(INTERFACE)
