# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_bar.Entity import DeltaEntity
from kusozako_bar.const import StatusNotifierSpec


class AlfaDBusSignalEmitter(DeltaEntity):

    __signal__ = "define signal to receive here."

    def _emit_signal(self, signal, param=None):
        connection = self._enquiry("delta > connection")
        connection.emit_signal(
            StatusNotifierSpec.WELL_KNOWN_NAME,
            StatusNotifierSpec.OBJECT_PATH,
            StatusNotifierSpec.INTERFACE,
            signal,
            param
            )

    def _on_signal_received(self, param):
        raise NotImplementedError

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != self.__signal__:
            return
        self._on_signal_received(param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register status notifier object", self)
