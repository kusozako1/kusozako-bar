# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import threading
from i3ipc import Connection
from i3ipc import Event


class FoxtrotIpc:

    def _on_event(self, connection, event):
        pass

    def get_connection(self):
        return self._connection

    def __init__(self):
        self._connection = Connection()
        self._connection.on(Event.WINDOW, self._on_event)
        self._connection.on(Event.WORKSPACE, self._on_event)
        self._connection.on(Event.MODE, self._on_event)
        thread = threading.Thread(target=self._connection.main, daemon=True)
        thread.start()
