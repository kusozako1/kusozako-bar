# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_bar.Entity import DeltaEntity


class DeltaLabel(Gtk.EventBox, DeltaEntity):

    @classmethod
    def new(cls, parent, workspace):
        instance = cls(parent)
        instance.construct(workspace)
        return instance

    def _reset_css(self):
        style_context = self.get_style_context()
        if self._workspace.focused:
            style_context.add_class("current-workspace")
        else:
            style_context.remove_class("current-workspace")

    def construct(self, workspace):
        self._label.set_text(workspace.name)
        self._workspace = workspace
        self._reset_css()

    def _on_button_press(self, *args):
        connection = self._enquiry("delta > connection")
        connection.command("workspace {}".format(self._workspace.name))

    def _on_enter_notify(self, *args):
        style_context = self.get_style_context()
        style_context.add_class("current-workspace")

    def _on_leave_notify(self, *args):
        self._reset_css()

    def __init__(self, parent):
        self._parent = parent
        Gtk.EventBox.__init__(self)
        self.connect("button-press-event", self._on_button_press)
        self.connect("enter-notify-event", self._on_enter_notify)
        self.connect("leave-notify-event", self._on_leave_notify)
        self.set_size_request(32, -1)
        self._label = Gtk.Label()
        self.add(self._label)
