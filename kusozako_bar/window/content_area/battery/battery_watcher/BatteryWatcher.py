# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_bar.Entity import DeltaEntity
from .UPowerProxy import DeltaUPowerProxy
from .BatteryDevice import DeltaBatteryDevice


class DeltaBatteryWatcher(DeltaEntity):

    def _delta_call_battery_device_found(self, device_proxy):
        DeltaBatteryDevice.new_for_proxy(self, device_proxy)

    def __init__(self, parent):
        self._parent = parent
        DeltaUPowerProxy(self)
