# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_bar.Entity import DeltaEntity


class DeltaBatteryDevice(DeltaEntity):

    @classmethod
    def new_for_proxy(cls, parent, device_proxy):
        instance = cls(parent)
        instance.construct(device_proxy)

    def _timeout(self, device_proxy):
        icon_name = device_proxy.get_cached_property("IconName").unpack()
        state = device_proxy.get_cached_property("State").unpack()
        percentage = device_proxy.get_cached_property("Percentage").unpack()
        user_data = state, icon_name, percentage
        self._raise("delta > battery property observed", user_data)
        return GLib.SOURCE_CONTINUE

    def construct(self, proxy):
        GLib.timeout_add_seconds(2, self._timeout, proxy)

    def __init__(self, parent):
        self._parent = parent
