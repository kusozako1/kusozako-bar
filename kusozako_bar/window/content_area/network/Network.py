# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako_bar.Entity import DeltaEntity
from .Observer import DeltaObserver

FLAGS = GLib.FormatSizeFlags.IEC_UNITS


class DeltaNetwork(Gtk.Box, DeltaEntity):

    def _delta_call_observed(self, user_data):
        sent_bytes, recv_bytes = user_data
        text = "{} / {} kbps".format(
            '{:,}'.format(round(sent_bytes*8/1000, 1)),
            '{:,}'.format(round(recv_bytes*8/1000, 1)),
            )
        self._label.set_label(text)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        image = Gtk.Image.new_from_icon_name(
            "network-wireless-symbolic",
            Gtk.IconSize.MENU
            )
        image.set_margin_end(4)
        self.add(image)
        self._label = Gtk.Label(margin_end=8)
        self.add(self._label)
        DeltaObserver(self)
