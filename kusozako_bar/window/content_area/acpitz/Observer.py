# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import psutil
from gi.repository import GLib
from kusozako_bar.Entity import DeltaEntity


class DeltaObserver(DeltaEntity):

    def _timeout(self):
        data = psutil.sensors_temperatures()
        critical = data["acpitz"][0].critical
        user_data = data["acpitz"][0].current, critical
        self._raise("delta > observed", user_data)
        return GLib.SOURCE_CONTINUE

    def __init__(self, parent):
        self._parent = parent
        GLib.timeout_add_seconds(1, self._timeout)
