# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako_bar.Entity import DeltaEntity
from .Observer import DeltaObserver

FLAGS = GLib.FormatSizeFlags.IEC_UNITS
SIZE = Gtk.IconSize.MENU


class DeltaAcpitz(Gtk.Box, DeltaEntity):

    def _delta_call_observed(self, user_data):
        current, critical = user_data
        state = 0.01 if critical is None else current/critical
        if state >= 0.8:
            self._image.set_from_icon_name("dialog-warning-symbolic", SIZE)
        else:
            self._image.set_from_icon_name("emblem-ok-symbolic", SIZE)
        text = "{}°C".format(current)
        self._label.set_label(text)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self._image = Gtk.Image.new_from_icon_name("emblem-ok-symbolic", SIZE)
        self._image.set_margin_end(4)
        self.add(self._image)
        self._label = Gtk.Label(margin_end=8)
        self.add(self._label)
        DeltaObserver(self)
