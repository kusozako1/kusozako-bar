# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_bar.Entity import DeltaEntity
from .IsStatusNotifierHostRegistered import DeltaIsStatusNotifierHostRegistered
from .RegisteredStatusNotifierItems import DeltaRegisteredStatusNotifierItems
from .ProtocolVersion import DeltaProtocolVersion


class DeltaProperties(DeltaEntity):

    def _delta_call_register_dbus_property(self, user_data):
        property_name, property_entity = user_data
        self._properties[property_name] = property_entity

    def dispatch(self, connection, bus, path, interface, property_):
        property_entity = self._properties[property_]
        return property_entity.get_property()

    def __init__(self, parent):
        self._parent = parent
        self._properties = {}
        DeltaRegisteredStatusNotifierItems(self)
        DeltaIsStatusNotifierHostRegistered(self)
        DeltaProtocolVersion(self)
