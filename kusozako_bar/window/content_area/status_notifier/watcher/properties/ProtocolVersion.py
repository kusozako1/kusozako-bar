# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_bar.alfa.DBusProperty import AlfaDBusProperty


class DeltaProtocolVersion(AlfaDBusProperty):

    __property_name__ = "ProtocolVersion"

    def get_property(self):
        return GLib.Variant.new_int32(0)
