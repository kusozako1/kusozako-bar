# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_bar.alfa.DBusProperty import AlfaDBusProperty
from kusozako_bar.const import StatusNotifierSignals


class DeltaIsStatusNotifierHostRegistered(AlfaDBusProperty):

    __property_name__ = "IsStatusNotifierHostRegistered"

    def get_property(self):
        return GLib.Variant.new_boolean(self._registered)

    def receive_transmission(self, user_data):
        signal, item_name = user_data
        if signal != StatusNotifierSignals.HOST_REGISTERED:
            return
        self._registered = True

    def _on_initialize(self):
        self._registered = False
        self._raise("delta > register status notifier object", self)
