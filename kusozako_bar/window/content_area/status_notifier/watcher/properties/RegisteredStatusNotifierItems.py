# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_bar.alfa.DBusProperty import AlfaDBusProperty
from kusozako_bar.const import StatusNotifierSignals


class DeltaRegisteredStatusNotifierItems(AlfaDBusProperty):

    __property_name__ = "RegisteredStatusNotifierItems"

    def get_property(self):
        return GLib.Variant.new_strv(self._registered_items)

    def receive_transmission(self, user_data):
        signal, item_name = user_data
        if signal != StatusNotifierSignals.ITEM_REGISTERED:
            return
        self._registered_items.append(item_name)

    def _on_initialize(self):
        self._registered_items = []
        self._raise("delta > register status notifier object", self)
