# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako_bar.Entity import DeltaEntity
from kusozako_bar.const import StatusNotifierSpec


class DeltaConnection(DeltaEntity):

    def _on_name_aquired(self, dbus_connection, name):
        self._raise("delta > connection built", dbus_connection)

    def _on_bus_got(self, cancellable, task):
        dbus_connection = Gio.bus_get_finish(task)
        Gio.bus_own_name_on_connection(
            dbus_connection,                        # dbus connection
            StatusNotifierSpec.WELL_KNOWN_NAME,     # dbus well known name
            Gio.BusNameOwnerFlags.NONE,             # call method closure
            self._on_name_aquired,                  # get property closure
            None,                                   # set property closure
            )

    def __init__(self, parent):
        self._parent = parent
        Gio.bus_get(Gio.BusType.SESSION, None, self._on_bus_got)
