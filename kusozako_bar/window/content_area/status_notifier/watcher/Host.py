# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import os
from gi.repository import Gio
from kusozako_bar.Entity import DeltaEntity
from kusozako_bar.const import StatusNotifierSignals

TEMPLATE = "org.freedesktop.StatusNotifierHost-{}-1"


class DeltaHost(DeltaEntity):

    def _on_name_aquired(self, connection, host_name):
        user_data = StatusNotifierSignals.HOST_REGISTERED, host_name
        self._raise("delta > status notifier signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        connection = self._enquiry("delta > connection")
        pid = str(os.getpid())
        Gio.bus_own_name_on_connection(
            connection,
            TEMPLATE.format(pid),
            Gio.BusNameOwnerFlags.NONE,
            self._on_name_aquired
            )
