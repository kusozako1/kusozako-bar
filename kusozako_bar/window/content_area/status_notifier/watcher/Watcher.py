# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_bar.Entity import DeltaEntity
from kusozako_bar.const import StatusNotifierSpec
from .Connection import DeltaConnection
from .methods.Methods import DeltaMethods
from .properties.Properties import DeltaProperties
from .signals.Signals import EchoSignals
from .Host import DeltaHost


class DeltaWatcher(DeltaEntity):

    def _delta_info_connection(self):
        return self._connection

    def _delta_call_connection_built(self, connection):
        self._connection = connection
        methods = DeltaMethods(self)
        properties = DeltaProperties(self)
        connection.register_object(
            StatusNotifierSpec.OBJECT_PATH,
            StatusNotifierSpec.INTERFACE_INFO,
            methods.dispatch,
            properties.dispatch,
            )
        DeltaHost(self)

    def __init__(self, parent):
        self._parent = parent
        EchoSignals(self)
        DeltaConnection(self)
