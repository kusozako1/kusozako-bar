# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_bar.Entity import DeltaEntity
from kusozako_bar.Transmitter import FoxtrotTransmitter
from .watcher.Watcher import DeltaWatcher
from .user_interface.UserInterface import DeltaUserInterface


class DeltaStatusNotifier(Gtk.Box, DeltaEntity):

    def _delta_call_register_status_notifier_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_status_notifier_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_add_to_container(self, widget):
        self.add(widget)
        self.show_all()

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        Gtk.Box.__init__(
            self,
            margin_end=8,
            orientation=Gtk.Orientation.HORIZONTAL,
            )
        DeltaUserInterface(self)
        DeltaWatcher(self)
