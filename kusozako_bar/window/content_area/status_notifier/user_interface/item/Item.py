# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio
from kusozako_bar.Entity import DeltaEntity
from .DBusProperty import FoxtrotDBusProperty


class DeltaItem(Gtk.Image, DeltaEntity):

    @classmethod
    def new(cls, parent, well_known_name):
        if "/" in well_known_name:
            return                          # to reject ayatana indicators
        instance = cls(parent)
        instance.construct(well_known_name)

    def _on_signal(self, *args):
        _, _, _, _, _, _, dbus_property = args
        icon_name = dbus_property.get_property("IconName")
        self.set_from_icon_name(icon_name, Gtk.IconSize.MENU)

    def construct(self, well_known_name):
        connection = Gio.bus_get_sync(Gio.BusType.SESSION)
        dbus_property = FoxtrotDBusProperty(connection, well_known_name)
        icon_name = dbus_property.get_property("IconName")
        self.set_from_icon_name(icon_name, Gtk.IconSize.MENU)
        connection.signal_subscribe(
            well_known_name,                # sender
            "org.kde.StatusNotifierItem",   # interface
            "NewIcon",                      # signal name
            "/StatusNotifierItem",          # object path
            None,                           # arg0 to natch on
            Gio.DBusSignalFlags.NONE,       # how to handle arg0
            self._on_signal,                # callback
            dbus_property,                  # user-data for callback
            )

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self, pixel_size=20)
        self._raise("delta > add to container", self)
