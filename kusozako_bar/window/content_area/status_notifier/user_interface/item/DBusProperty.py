# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib


class FoxtrotDBusProperty:

    def get_property(self, property_name):
        argument = GLib.Variant.new_tuple(
            GLib.Variant.new_string("org.kde.StatusNotifierItem"),
            GLib.Variant.new_string(property_name),
            )
        glib_variant = self._property_proxy.call_sync(
            "Get",                                  # method name
            argument,                               # argument for method
            Gio.DBusCallFlags.NONE,                 # flags for call
            -1,                                     # timeout. -1 is default
            None,                                   # cancellable
            )
        return glib_variant.unpack()[0]

    def __init__(self, connection, well_known_name):
        self._property_proxy = Gio.DBusProxy.new_sync(
            connection,                             # connection
            Gio.DBusProxyFlags.NONE,                # flag for constructing
            None,                                   # dbus interface info
            well_known_name,                        # bus name
            "/StatusNotifierItem",                  # object path
            "org.freedesktop.DBus.Properties",      # interface
            None,                                   # cancellable
            )
