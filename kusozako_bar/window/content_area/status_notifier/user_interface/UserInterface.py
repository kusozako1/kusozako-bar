# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_bar.Entity import DeltaEntity
from kusozako_bar.const import StatusNotifierSignals
from .item.Item import DeltaItem


class DeltaUserInterface(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == StatusNotifierSignals.ITEM_REGISTERED:
            DeltaItem.new(self, param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register status notifier object", self)
