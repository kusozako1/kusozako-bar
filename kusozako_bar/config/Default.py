# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later


DEFAULT = """
{
    "width": -1,
    "height": 32,
    "edge": ["bottom"],
    "foreground-color": "White",
    "shadow-color": "Transparent",
    "background-color": "alpha(Black, 0.5)",
    "opacity": 0.5,
    "interval": 2,
    "center-item": "active-window",
    "left-items": [
        "mode",
        "workspaces"
        ],
    "right-items": [
        "clock",
        "battery",
        "network",
        "status-notifier"
        ]
}
"""
