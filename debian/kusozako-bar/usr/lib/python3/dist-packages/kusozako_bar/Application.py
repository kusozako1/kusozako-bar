# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import Gtk
from kusozako_bar import APPLICATION_ID
from kusozako_bar.Entity import DeltaEntity
from .window.Window import DeltaWindow
from .config.Config import KusozakoConfig
from .css.Css import KusozakoCss
from .ipc.Ipc import FoxtrotIpc


class DeltaApplication(Gtk.Application, DeltaEntity):

    def _on_activate(self, application):
        KusozakoConfig.init()
        DeltaWindow(self)
        KusozakoCss()

    def _delta_call_add_to_container(self, widget):
        self.add_window(widget)

    def _request_synchronization(self, message, user_data=None):
        print("delta > message uncaught", message, user_data)

    def _delta_info_connection(self):
        return self._ipc.get_connection()

    def __init__(self):
        self._parent = None
        self._ipc = FoxtrotIpc()
        Gtk.Application.__init__(
            self,
            application_id=APPLICATION_ID,
            flags=Gio.ApplicationFlags.FLAGS_NONE,
            )
        self.connect("activate", self._on_activate)
