# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_bar.Entity import DeltaEntity
from .battery_watcher.BatteryWatcher import DeltaBatteryWatcher


class DeltaBattery(Gtk.Box, DeltaEntity):

    def _delta_call_battery_property_observed(self, user_data):
        state, icon_name, percentage = user_data
        self._image.set_from_icon_name(icon_name, Gtk.IconSize.MENU)
        self._label.set_text("{} %".format(int(percentage)))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            margin_end=8,
            )
        self._image = Gtk.Image(margin_end=4)
        self.add(self._image)
        self._label = Gtk.Label()
        self.add(self._label)
        DeltaBatteryWatcher(self)
