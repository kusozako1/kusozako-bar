# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .StatusNotifierHostRegistered import DeltaStatusNotifierHostRegistered
from .StatusNotifierItemRegistered import DeltaStatusNotifierItemRegistered


class EchoSignals:

    def __init__(self, parent):
        DeltaStatusNotifierHostRegistered(parent)
        DeltaStatusNotifierItemRegistered(parent)
