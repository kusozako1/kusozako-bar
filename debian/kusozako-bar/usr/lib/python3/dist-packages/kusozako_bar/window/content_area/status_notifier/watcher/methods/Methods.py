# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_bar.Entity import DeltaEntity
from kusozako_bar.Transmitter import FoxtrotTransmitter
from .RegisterStatusNotifierItem import DeltaRegisterStatusNotifierItem


class DeltaMethods(DeltaEntity):

    def _delta_call_register_dbus_method_object(self, object_):
        self._transmitter.register_listener(object_)

    def dispatch(self, *args):
        _, _, _, _, method, param, invocation = args
        user_data = method, param, invocation
        self._transmitter.transmit(user_data)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        DeltaRegisterStatusNotifierItem(self)
