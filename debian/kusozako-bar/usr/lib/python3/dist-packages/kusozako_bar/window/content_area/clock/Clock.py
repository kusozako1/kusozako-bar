# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako_bar.Entity import DeltaEntity


class DeltaClock(Gtk.EventBox, DeltaEntity):

    def _timeout(self):
        date_time = GLib.DateTime.new_now_local()
        if self._contain_pointer:
            text = date_time.format("%F (%A)")
        else:
            text = date_time.format("%R")
        self._label.set_text(text)
        return GLib.SOURCE_CONTINUE

    def _on_event(self, event_box, event, contain_pointer):
        self._contain_pointer = contain_pointer
        self._timeout()

    def __init__(self, parent):
        self._parent = parent
        self._contain_pointer = False
        Gtk.EventBox.__init__(self)
        self.connect("enter-notify-event", self._on_event, True)
        self.connect("leave-notify-event", self._on_event, False)
        self._label = Gtk.Label(margin_end=8)
        self._timeout()
        GLib.timeout_add_seconds(2, self._timeout)
        self.add(self._label)
