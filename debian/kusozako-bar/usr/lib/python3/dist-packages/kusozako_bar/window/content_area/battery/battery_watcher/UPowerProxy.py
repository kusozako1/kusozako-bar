# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako_bar.Entity import DeltaEntity


class DeltaUPowerProxy(DeltaEntity):

    def _on_device_ready(self, source_object, task, user_data=None):
        device_proxy = Gio.DBusProxy.new_finish(task)
        if device_proxy.get_cached_property("Type").get_uint32() == 2:
            self._raise("delta > battery device found", device_proxy)

    def _on_upower_ready(self, source_object, task, user_data=None):
        upower_proxy = Gio.DBusProxy.new_finish(task)
        for object_path in upower_proxy.EnumerateDevices():
            Gio.DBusProxy.new_for_bus(
                Gio.BusType.SYSTEM,
                Gio.DBusProxyFlags.NONE,
                None,
                "org.freedesktop.UPower",
                object_path,
                "org.freedesktop.UPower.Device",
                None,
                self._on_device_ready,
                None,
                )

    def __init__(self, parent):
        self._parent = parent
        Gio.DBusProxy.new_for_bus(
            Gio.BusType.SYSTEM,         # connection. SYSTEM or USER
            Gio.DBusProxyFlags.NONE,    # flags
            None,                       # interface info
            "org.freedesktop.UPower",   # well known name
            "/org/freedesktop/UPower",  # object path
            "org.freedesktop.UPower",   # interface
            None,                       # Gio.Cancellable
            self._on_upower_ready,      # upower proxy ready callback
            None,                       # user_data for callback
            )
