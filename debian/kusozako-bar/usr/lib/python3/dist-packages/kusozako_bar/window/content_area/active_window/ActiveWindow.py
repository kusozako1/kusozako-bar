# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from i3ipc import Event
from gi.repository import Gtk
from gi.repository import GLib
from gi.repository import Pango
from kusozako_bar.Entity import DeltaEntity


class DeltaActiveWindow(Gtk.Label, DeltaEntity):

    def _get_title(self, focused):
        if focused is None:
            return ""
        if focused.type == "workspace":
            return ""
        title = focused.name
        return "" if title is None else title

    def _set_window_title(self, connection):
        tree = connection.get_tree()
        focused = tree.find_focused()
        title = self._get_title(focused)
        self.set_text(title)

    def _on_event(self, connection, event):
        GLib.idle_add(self._set_window_title, connection)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            hexpand=True,
            ellipsize=Pango.EllipsizeMode.MIDDLE,
            margin_end=8,
            )
        connection = self._enquiry("delta > connection")
        connection.on(Event.WINDOW, self._on_event)
        connection.on(Event.WORKSPACE, self._on_event)
        GLib.idle_add(self._set_window_title, connection)
