# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from gi.repository import GtkLayerShell
from kusozako_bar.const import ConfigKeys
from kusozako_bar.config.Config import KusozakoConfig
from kusozako_bar import APPLICATION_ID
from kusozako_bar.Entity import DeltaEntity

EDGES = {
    "top": GtkLayerShell.Edge.TOP,
    "bottom": GtkLayerShell.Edge.BOTTOM,
    "left": GtkLayerShell.Edge.LEFT,
    "right": GtkLayerShell.Edge.RIGHT,
}


class DeltaLayerShell(DeltaEntity):

    def _set_size(self, config, window):
        display = Gdk.Display.get_default()
        monitor = display.get_monitor_at_window(window.get_window())
        rectangle = monitor.get_workarea()
        width = config[ConfigKeys.WIDTH]
        height = config[ConfigKeys.HEIGHT]
        if width == -1:
            width = rectangle.width
        if height == -1:
            height = rectangle.height
        window.set_size_request(width, height)

    def _set_edge(self, config, window):
        for edge_position in config[ConfigKeys.EDGE]:
            GtkLayerShell.set_anchor(window, EDGES[edge_position], True)

    def _on_realize(self, window):
        GtkLayerShell.init_for_window(window)
        GtkLayerShell.set_layer(window, GtkLayerShell.Layer.BOTTOM)
        GtkLayerShell.set_namespace(window, APPLICATION_ID)
        GtkLayerShell.auto_exclusive_zone_enable(window)
        config = KusozakoConfig.get_default()
        self._set_size(config, window)
        self._set_edge(config, window)

    def __init__(self, parent):
        self._parent = parent
        window = self._enquiry("delta > window")
        window.connect("realize", self._on_realize)
