# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import importlib
from gi.repository import Gtk
from kusozako_bar.Entity import DeltaEntity
from kusozako_bar.const import ConfigKeys
from kusozako_bar.config.Config import KusozakoConfig


class DeltaContentArea(Gtk.Box, DeltaEntity):

    def _get_camel_case(self, text):
        camel_case = ""
        for element in text.split("-"):
            camel_case += element.capitalize()
        return camel_case

    def _get_item(self, item_name):
        snake_case = item_name.replace("-", "_")
        camel_case = self._get_camel_case(item_name)
        module_name = __package__+".{}.{}".format(snake_case, camel_case)
        class_name = "Delta"+camel_case
        class_ = getattr(importlib.import_module(module_name), class_name)
        return class_(self)

    def _set_left_items(self, config):
        for item_name in config[ConfigKeys.LEFT_ITEMS]:
            item = self._get_item(item_name)
            self.pack_start(item, False, False, 0)

    def _set_center_item(self, config):
        item_name = config[ConfigKeys.CENTER_ITEM]
        item = self._get_item(item_name)
        self.set_center_widget(item)

    def _set_right_items(self, config):
        for item_name in config[ConfigKeys.RIGHT_ITEMS]:
            item = self._get_item(item_name)
            self.pack_end(item, False, False, 0)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        config = KusozakoConfig.get_default()
        self._set_left_items(config)
        self._set_center_item(config)
        self._set_right_items(config)
        self._raise("delta > add to container", self)
