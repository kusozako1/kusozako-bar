# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from i3ipc import Event
from gi.repository import Gtk
from gi.repository import GLib
from kusozako_bar.Entity import DeltaEntity
from .label.Label import DeltaLabel


class DeltaWorkspaces(Gtk.Box, DeltaEntity):

    def _destroy_all(self):
        for child in self.get_children():
            child.destroy()

    def _set_workspaces(self, connection):
        self._destroy_all()
        for workspace in connection.get_workspaces():
            label = DeltaLabel.new(self, workspace)
            self.add(label)
        self.show_all()

    def _on_event(self, connection, event):
        GLib.idle_add(self._set_workspaces, connection)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        connection = self._enquiry("delta > connection")
        connection.on(Event.WORKSPACE, self._on_event)
        GLib.idle_add(self._set_workspaces, connection)
