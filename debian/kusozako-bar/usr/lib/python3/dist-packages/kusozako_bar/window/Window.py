# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_bar.Entity import DeltaEntity
from .LayerShell import DeltaLayerShell
from .content_area.ContentArea import DeltaContentArea


class DeltaWindow(Gtk.Window, DeltaEntity):

    def _delta_info_window(self):
        return self

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Window.__init__(self)
        DeltaLayerShell(self)
        DeltaContentArea(self)
        self._raise("delta > add to container", self)
        self.show_all()
