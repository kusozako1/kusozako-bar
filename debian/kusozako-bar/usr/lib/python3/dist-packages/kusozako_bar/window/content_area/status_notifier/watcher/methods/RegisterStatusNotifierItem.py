# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_bar.alfa.DBusMethod import AlfaDBusMethod
from kusozako_bar.const import StatusNotifierSignals


class DeltaRegisterStatusNotifierItem(AlfaDBusMethod):

    __method_name__ = "RegisterStatusNotifierItem"

    def _invoke(self, param, invocation):
        well_known_name = param.unpack()[0]
        invocation.return_value(None)
        user_data = StatusNotifierSignals.ITEM_REGISTERED, well_known_name
        self._raise("delta > status notifier signal", user_data)
