# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_bar.const import StatusNotifierSignals
from kusozako_bar.alfa.DBusSignalEmitter import AlfaDBusSignalEmitter


class DeltaStatusNotifierHostRegistered(AlfaDBusSignalEmitter):

    __signal__ = StatusNotifierSignals.HOST_REGISTERED

    def _on_signal_received(self, host_name):
        self._emit_signal("StatusNotifierHostRegistered", None)
