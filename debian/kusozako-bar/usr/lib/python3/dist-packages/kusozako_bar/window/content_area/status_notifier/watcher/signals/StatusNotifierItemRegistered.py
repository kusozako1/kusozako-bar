# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_bar.const import StatusNotifierSignals
from kusozako_bar.alfa.DBusSignalEmitter import AlfaDBusSignalEmitter


class DeltaStatusNotifierItemRegistered(AlfaDBusSignalEmitter):

    __signal__ = StatusNotifierSignals.ITEM_REGISTERED

    def _on_signal_received(self, item_name):
        glib_variant = GLib.Variant.new_tuple(
            GLib.Variant.new_string(item_name),
            )
        self._emit_signal("StatusNotifierItemRegistered", glib_variant)
