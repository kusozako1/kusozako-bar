# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from i3ipc import Event
from gi.repository import Gtk
from gi.repository import GLib
from kusozako_bar.Entity import DeltaEntity

TEMPLATE = "  Mode : {}  "


class DeltaMode(Gtk.Label, DeltaEntity):

    def _set_mode(self, connection, event):
        mode = event.change
        text = "" if mode == "default" else TEMPLATE.format(mode)
        self.set_text(text)

    def _on_event(self, connection, event):
        GLib.idle_add(self._set_mode, connection, event)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        style_context = self.get_style_context()
        style_context.add_class("mode-label")
        connection = self._enquiry("delta > connection")
        connection.on(Event.MODE, self._on_event)
