# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import psutil
from gi.repository import GLib
from kusozako_bar.Entity import DeltaEntity


class DeltaObserver(DeltaEntity):

    def _timeout(self):
        data = psutil.net_io_counters()
        total_sent = data.bytes_sent
        total_recv = data.bytes_recv
        if self._sent != 0:
            current_sent = total_sent - self._sent
            current_recv = total_recv - self._recv
            user_data = current_sent, current_recv
            self._raise("delta > observed", user_data)
        self._sent = total_sent
        self._recv = total_recv
        return GLib.SOURCE_CONTINUE

    def __init__(self, parent):
        self._parent = parent
        self._sent = 0
        self._recv = 0
        GLib.timeout_add_seconds(1, self._timeout)
