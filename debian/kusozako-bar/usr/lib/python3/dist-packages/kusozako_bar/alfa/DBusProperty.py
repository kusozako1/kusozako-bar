# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_notifications.Entity import DeltaEntity


class AlfaDBusProperty(DeltaEntity):

    __property_name__ = "define property name here."

    def get_property(self, param):
        raise NotImplementedError

    def _on_initialize(self):
        pass

    def __init__(self, parent):
        self._parent = parent
        self._on_initialize()
        user_data = self.__property_name__, self
        self._raise("delta > register dbus property", user_data)
