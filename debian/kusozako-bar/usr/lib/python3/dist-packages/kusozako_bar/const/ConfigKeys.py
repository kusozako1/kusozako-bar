# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

WIDTH = "width"
HEIGHT = "height"
EDGE = "edge"
CENTER_ITEM = "center-item"
LEFT_ITEMS = "left-items"
RIGHT_ITEMS = "right-items"
SHADOW_COLOR = "shadow-color"
BACKGROUND_COLOR = "background-color"
FOREGROUND_COLOR = "foreground-color"
