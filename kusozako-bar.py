#!/usr/bin/env python3

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
from kusozako_bar.Application import DeltaApplication

if __name__ == "__main__":
    application = DeltaApplication()
    exit_status = application.run(sys.argv)
    sys.exit(exit_status)
